


//Add 5 courses in a new courses collection in the QUERY-176 DB with the following details

db.courses.insertMany([

	{
		"name": "HTML Basics",
		"price": 20000,
		"isActive": true,
		"instructor": "Sir Marty"

	},
		{
		"name": "CSS 101 + Flexbox",
		"price": 21000,
		"isActive": true,
		"instructor": "Sir Marty"

	},
		{
		"name": "Javascript 101",
		"price": 32000,
		"isActive": true,
		"instructor": "Ma'am Joy"

	},
		{
		"name": "Git 101, IDE and CLI",
		"price": 19000,
		"isActive": false,
		"instructor": "Ma'am Joy"

	},
		{
		"name": "React.JS 101",
		"price": 25000,
		"isActive": true,
		"instructor": "Ma'am Miah"

	}

	])


// 1.)
db.courses.find(
	{$and:
[
		{instructor:"Sir Marty"},
		{price:{$gte:20000}}
]
},
{
	"_id": 0,
	"name": 1,
	"price": 1
}
)

// 2.)

db.courses.find({$and:
[
		{instructor:"Ma'am Joy"},
		{isActive:false}
]
},
{
	"_id": 0,
	"name": 1,
	"price": 1
}
)


// 3.)
db.courses.find({$and:[{name:{$regex:'r',$options:'$i'}}, {price:{$lte:25000}}]})

// 4.)
db.courses.updateMany({price: {$lt:21000}}, {$set:{isActive:false}})

// 5.)
db.courses.deleteMany({price: {$gte:25000}})
